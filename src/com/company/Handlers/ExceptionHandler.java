package com.company.Handlers;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;

/**
 * Created by titas on 2017-04-08.
 */
public class ExceptionHandler {
    public void CheckIfStartEventsAreValid(List<Node> nodeList) throws CustomException {
        if (nodeList.size() == 0) {
            throw new CustomException("There is no start event");
        }
        if (nodeList.size() > 1) {
            throw new CustomException("More than one start event was found");
        }
    }

    public void ThrowCustomException(String exception) throws CustomException {
        throw new CustomException(exception);
    }

    public void CheckIfExclusiveGatewayIsCorrect(long ifcount, long elseifcount, long elsecount) throws CustomException {
        if (elseifcount > 0 && ifcount > 1 || elsecount > 1) {
            throw new CustomException("Something wrong with one gateway, cannot be more than one condition or otherwise outgoing elements when exists outgoing element condition with statement");
        }
        if (elsecount > 1) {
            throw new CustomException("Something wrong with one gateway, cannot be more than one otherwise outgoing element");
        }
        if (ifcount > 1 && elsecount > 0) {
            throw new CustomException("Something wrong with one gateway, cannot exist otherwise outgoing element when more than one condition outgoing element exist");
        }
    }

    public class CustomException extends Exception
    {
        //Parameterless Constructor
        public CustomException() {}

        //Constructor that accepts a message
        public CustomException(String message)
        {
            super(message);
        }
    }
}
