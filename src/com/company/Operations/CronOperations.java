package com.company.Operations;

import com.company.Storage.Storage;
import org.w3c.dom.NodeList;
import org.w3c.dom.*;

/**
 * Created by titas on 2017-04-12.
 */
public class CronOperations {
    GlobalOperations globalOperations = new GlobalOperations();

    public void ProcessCustomCron(Node currentNode, Storage codeBuilder) {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String secondsValue = "";
        String minutesValue = "";
        String hoursValue = "";
        String dayValue = "";
        String monthValue = "";
        String dayOfWeekValue = "";
        String yearValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "seconds":
                    secondsValue = node.getTextContent().trim();
                    break;
                case "minutes":
                    minutesValue = node.getTextContent().trim();
                    break;
                case "hours":
                    hoursValue = node.getTextContent().trim();
                    break;
                case "day":
                    dayValue = node.getTextContent().trim();
                    break;
                case "month":
                    monthValue = node.getTextContent().trim();
                    break;
                case "dayofweek":
                    dayOfWeekValue = node.getTextContent().trim();
                    break;
                case "year":
                    yearValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }
        String cronExpression;
        if (!yearValue.isEmpty()) {
            cronExpression = "Time cron \"" + secondsValue + " " + minutesValue + " " + hoursValue + " " + dayValue + " " + monthValue + " " + dayOfWeekValue + " " + yearValue + "\"";
        } else {
            cronExpression = "Time cron \"" + secondsValue + " " + minutesValue + " " + hoursValue + " " + dayValue + " " + monthValue + " " + dayOfWeekValue + "\"";
        }
        codeBuilder.AppendString(cronExpression);
    }
}
