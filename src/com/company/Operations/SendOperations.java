package com.company.Operations;

import com.company.Constants.SubprocessTypes;
import com.company.Handlers.ExceptionHandler;
import com.company.Storage.Storage;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by titas on 2017-04-13.
 */
public class SendOperations {
    GlobalOperations globalOperations = new GlobalOperations();
    ExceptionHandler exceptionHandler = new ExceptionHandler();

    public void ProcessSendCommand(Node currentNode, Storage codeBuilder, Storage globalVariables) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String itemNameValue = "";
        String commandValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "itemname":
                    itemNameValue = node.getTextContent().trim();
                    break;
                case "command":
                    commandValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //expected itemName and typeValue are split with | in dropdown values.
        String[] nameAndType = itemNameValue.split("\\|");
        String itemName = nameAndType[0];
        String itemType = nameAndType[1];
        //check if variable exists, when trying to use existing variable with specified @
        if (commandValue.startsWith("@")) {
            commandValue = commandValue.replaceAll("@", "");
            globalOperations.ValidateVariableExistance(commandValue, codeBuilder, globalVariables);
        } else {
            //if not a varaible check if value is allowed for that item type
            globalOperations.CheckIfGivenValueIsAllowedForItemType(itemType, commandValue, itemName, currentNode);
        }

        String sendCommandExpression = "sendCommand(" + itemName + ", " + commandValue + ")";
        codeBuilder.AppendString(sendCommandExpression);
    }

    public void ProcessSendCommandToCurrentItem(Node currentNode, Storage codeBuilder, Storage globalVariables) throws ExceptionHandler.CustomException {
        //check if used in the current iteration of the loop.
        Node parentNode = currentNode.getParentNode();
        NamedNodeMap attributes = parentNode.getAttributes();
        Node templateAttributeNode = attributes.getNamedItem("camunda:modelerTemplate");
        if (templateAttributeNode == null || !templateAttributeNode.getNodeValue().equals(SubprocessTypes.foreachsubprocess)) {
            String elementNodeName = currentNode.getNodeName().trim();
            NamedNodeMap attributesOfCurrentNode = currentNode.getAttributes();
            Node idAttributeNode = attributesOfCurrentNode.getNamedItem("id");
            String idValue = idAttributeNode.getNodeValue().trim();
            exceptionHandler.ThrowCustomException("Element: " + elementNodeName + " with id: " + idValue + " was used outside foreach loop. In order to send command " +
                    "to the item of current iteration, you must use it inside foreach loop.");
        }


        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String itemNameValue = "";
        String commandValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "itemnameiniteration":
                    itemNameValue = node.getTextContent().trim();
                    break;
                case "command":
                    commandValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //check if variable exists, when trying to use existing variable with specified @
        if (commandValue.startsWith("@")) {
            commandValue = commandValue.replaceAll("@", "");
            globalOperations.ValidateVariableExistance(commandValue, codeBuilder, globalVariables);
        }

        String sendCommandToCurrentItemExpression = "sendCommand(" + itemNameValue + ", " + commandValue + ")";
        codeBuilder.AppendString(sendCommandToCurrentItemExpression);
    }

    public void ProcessPostUpdate(Node currentNode, Storage codeBuilder, Storage globalVariables) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String itemNameValue = "";
        String updateValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "itemname":
                    itemNameValue = node.getTextContent().trim();
                    break;
                case "updatevalue":
                    updateValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }


        //expected itemName and typeValue are split with | in dropdown values.
        String[] nameAndType = itemNameValue.split("\\|");
        String itemName = nameAndType[0];
        String itemType = nameAndType[1];
        //check if variable exists, when trying to use existing variable with specified @
        if (updateValue.startsWith("@")) {
            updateValue = updateValue.replaceAll("@", "");
            globalOperations.ValidateVariableExistance(updateValue, codeBuilder, globalVariables);
        } else {
            //if not a varaible check if value is allowed for that item type
            globalOperations.CheckIfGivenValueIsAllowedForItemType(itemType, updateValue, itemName, currentNode);
        }

        String postUpdateExpression = "postUpdate(" + itemName + ", " + updateValue + ")";
        codeBuilder.AppendString(postUpdateExpression);
    }

    public void ProcessPostUpdateToCurrentItem(Node currentNode, Storage codeBuilder, Storage globalVariables) throws ExceptionHandler.CustomException {
        //check if used in the current iteration of the loop.
        Node parentNode = currentNode.getParentNode();
        NamedNodeMap attributes = parentNode.getAttributes();
        Node templateAttributeNode = attributes.getNamedItem("camunda:modelerTemplate");
        if (templateAttributeNode == null || !templateAttributeNode.getNodeValue().equals(SubprocessTypes.foreachsubprocess)) {
            String elementNodeName = currentNode.getNodeName().trim();
            NamedNodeMap attributesOfCurrentNode = currentNode.getAttributes();
            Node idAttributeNode = attributesOfCurrentNode.getNamedItem("id");
            String idValue = idAttributeNode.getNodeValue().trim();
            exceptionHandler.ThrowCustomException("Element: " + elementNodeName + " with id: " + idValue + " was used outside foreach loop. In order to post update " +
                    "to the item of current iteration, you must use it inside foreach loop.");
        }


        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String itemNameValue = "";
        String updateValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "itemnameiniteration":
                    itemNameValue = node.getTextContent().trim();
                    break;
                case "updatevalue":
                    updateValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //check if variable exists, when trying to use existing variable with specified @
        if (updateValue.startsWith("@")) {
            updateValue = updateValue.replaceAll("@", "");
            globalOperations.ValidateVariableExistance(updateValue, codeBuilder, globalVariables);
        }

        String postUpdateToCurrentItemExpression = "postUpdate(" + itemNameValue + ", " + updateValue + ")";
        codeBuilder.AppendString(postUpdateToCurrentItemExpression);
    }

    public void ProcessSendEmail(Node currentNode, Storage codeBuilder) {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String emailValue = "";
        String subjectValue = "";
        String messageValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "email":
                    emailValue = node.getTextContent().trim();
                    break;
                case "subject":
                    subjectValue = node.getTextContent().trim();
                    break;
                case "message":
                    messageValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }
        String mailExpression = "sendMail(\"" + emailValue + "\", \"" + subjectValue + "\", \"" + messageValue + "\")";
        codeBuilder.AppendString(mailExpression);
    }

    public void ProcessCallScript(Node currentNode, Storage codeBuilder) {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String scriptNameValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "scriptname":
                    scriptNameValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }
        String callScriptExpression = "callScript(\"" + scriptNameValue + "\")";
        codeBuilder.AppendString(callScriptExpression);
    }

    public void ProcessLogInfo(Node currentNode, Storage codeBuilder, Storage globalVariables) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String logMessageNameValue = "";
        String logMessageValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "logmessagename":
                    logMessageNameValue = node.getTextContent().trim();
                    break;
                case "logmessage":
                    logMessageValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //check if variable exists, when trying to use existing variable with specified @
        if (logMessageValue.startsWith("@")) {
            logMessageValue = logMessageValue.replaceAll("@", "");
            globalOperations.ValidateVariableExistance(logMessageValue, codeBuilder, globalVariables);
        }


        String logMessageExpression = "logInfo(\"" + logMessageNameValue + "\", " + logMessageValue + ")";
        codeBuilder.AppendString(logMessageExpression);
    }

    public void ProcessPlaySound(Node currentNode, Storage codeBuilder) {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String soundFileValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "soundfile":
                    soundFileValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        String playSoundExpression = "playSound(\"" + soundFileValue + "\")";
        codeBuilder.AppendString(playSoundExpression);
    }
}
