package com.company.Operations;

import com.company.Handlers.ExceptionHandler;
import com.company.Storage.Storage;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by titas on 2017-04-25.
 */
public class IntermediateCatchEventOperations {
    GlobalOperations globalOperations = new GlobalOperations();

    public void ProcessIntermediateTimeCatchEvent(Node currentNode, Storage codeBuilder) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String numberValue = "";
        String unitValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "number":
                    numberValue = node.getTextContent().trim();
                    break;
                case "unit":
                    unitValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        codeBuilder.AppendString("(now."+ unitValue + "(" + numberValue + ")) [|");
    }
}
