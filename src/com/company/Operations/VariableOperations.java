package com.company.Operations;

import com.company.Handlers.ExceptionHandler;
import com.company.Storage.Storage;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by titas on 2017-04-16.
 */
public class VariableOperations {
    GlobalOperations globalOperations = new GlobalOperations();
    ExceptionHandler exceptionHandler = new ExceptionHandler();

    public void ProcessPredifinedTimeVariable(Node currentNode, Storage codeBuilder, Storage globalVariables, Storage imports) {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String variableNameValue = "";
        String variableValue = "";
        String importValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "variablename":
                    variableNameValue = node.getTextContent().trim();
                    break;
                case "variablevalue":
                    variableValue = node.getTextContent().trim();
                    break;
                case "importvalue":
                    importValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }
        //add import
        String importSection = imports.GetBuiltString();
        if (importValue != "" && !importSection.contains(importValue)) {
            imports.AppendString(importValue);
        }

        //check if that variable was defined before
        String existingRuleCode = codeBuilder.GetBuiltString();
        if (variableNameValue != "" && existingRuleCode.contains("var " + variableNameValue)) {
            String variableAssignExpression = variableNameValue + " = " + variableValue;
            codeBuilder.AppendString(variableAssignExpression);
        } else {
            String variableAssignExpression = "var " + variableNameValue + " = " + variableValue;
            codeBuilder.AppendString(variableAssignExpression);
        }
    }

    public void ProcessCreateVariable(Node currentNode, Storage codeBuilder, Storage globalVariables, Storage imports) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String variableNameValue = "";
        String variableTypeValue = "";
        String variableValue = "";
        Boolean isGlobal = false;
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "variablename":
                    variableNameValue = node.getTextContent().trim();
                    break;
                case "variabletype":
                    variableTypeValue = node.getTextContent().trim();
                    break;
                case "variablevalue":
                    variableValue = node.getTextContent().trim();
                    break;
                case "isglobal":
                    isGlobal = Boolean.valueOf(node.getTextContent().trim());
                    break;
                default:
                    break;
            }
        }
        //check if variable global and can be global
        if (isGlobal && variableTypeValue == "") {
            exceptionHandler.ThrowCustomException("Variable: " + variableNameValue + " cannot be global because it does not have value type selected.");
        }

        //check if variable already defined
        String regexPattern = "\\b"+variableNameValue+"\\b";
        Boolean variableAlreadyExists = globalOperations.CheckIfExistsVariableByPattern(regexPattern, codeBuilder, globalVariables);
        if (variableAlreadyExists) {
            exceptionHandler.ThrowCustomException("Variable with name: " + variableNameValue + " already defined or any other object exists with that name.");
        }

        //check if variable exists, when trying to use existing variable with specified @
        if (variableValue.startsWith("@")) {
            variableValue = variableValue.replaceAll("@", "");
            globalOperations.ValidateVariableExistance(variableValue, codeBuilder, globalVariables);
        }

        //assign variables
        if (isGlobal) {
            String globalVariableAssignment = "var " + variableTypeValue + " " + variableNameValue;
            globalVariables.AppendString(globalVariableAssignment);
            String valueAssignment = variableNameValue + " = " + variableValue;
            codeBuilder.AppendString(valueAssignment);
        } else if (variableTypeValue == "") {
            String variableCreateAndAssignment = "var " + variableNameValue + " = " + variableValue;
            codeBuilder.AppendString(variableCreateAndAssignment);
        } else {
            String variableCreateAndAssignment = "var " + variableTypeValue + " " + variableNameValue + " = " + variableValue;
            codeBuilder.AppendString(variableCreateAndAssignment);
        }
    }

    public void ProcessAssignVariable(Node currentNode, Storage codeBuilder, Storage globalVariables, Storage imports) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String variableNameValue = "";
        String variableValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "variablename":
                    variableNameValue = node.getTextContent().trim();
                    break;
                case "variablevalue":
                    variableValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //check if variable already defined
        String regexPattern = "\\bvar \\b\\w+\\b " + variableNameValue + "\\b|\\bvar " + variableNameValue + "\\b";
        Boolean variableAlreadyExists = globalOperations.CheckIfExistsVariableByPattern(regexPattern, codeBuilder, globalVariables);
        if (!variableAlreadyExists) {
            exceptionHandler.ThrowCustomException("Variable with name: " + variableNameValue + " is not defined yet. Before assigning value to a variable, you must create it.");
        }

        //check if variable exists, when trying to use existing variable with specified @
        if (variableValue.startsWith("@")) {
            variableValue = variableValue.replaceAll("@", "");
            globalOperations.ValidateVariableExistance(variableValue, codeBuilder, globalVariables);
        }

        String variableAssignExpression = variableNameValue + " = " + variableValue;
        codeBuilder.AppendString(variableAssignExpression);
    }
}
