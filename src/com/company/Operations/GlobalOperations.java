package com.company.Operations;

import com.company.Constants.ItemTypes;
import com.company.Handlers.ExceptionHandler;
import com.company.Storage.Storage;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by titas on 2017-04-12.
 */
public class GlobalOperations {
    ExceptionHandler exceptionHandler = new ExceptionHandler();

    public void ProcessGivenCodeFromDropdown(Node currentNode, Storage codeBuilder) {
        NodeList inputParameters = GetChildElementsByTag(currentNode, "camunda:inputParameter");
        //will be always one when code is given
        codeBuilder.AppendString(inputParameters.item(0).getTextContent().trim());
    }

    public NodeList GetChildElementsByTag(Node node, String tag) {
        NodeList childNodes = node.getChildNodes();
        Element element = (Element)childNodes;
        return element.getElementsByTagName(tag);
    }

    public String GetAttributeValueByName(Node node, String attributeName) {
        NamedNodeMap attributesOfNode = node.getAttributes();
        Node nameAtrributeNode = attributesOfNode.getNamedItem(attributeName);
        return nameAtrributeNode.getNodeValue().trim();
    }

    public void CheckIfGivenValueIsAllowedForItemType(String itemType, String givenValue, String itemName, Node node) throws ExceptionHandler.CustomException {
        switch (itemType) {
            case ItemTypes.COLOR: {
                boolean isGivenValueCorrect = givenValue.matches("^ON$|^OFF$|^INCREASE$|^DECREASE$");
                boolean isPercentValueCorrect = givenValue.matches("^[1-9][0-9]?$|^100$|^[0]$");
                String givenValueWithoutQuotes = givenValue.replaceAll("^\"|\"$", "");
                String[] splittedValuesByComma = givenValueWithoutQuotes.split(",");
                boolean isHSBStringCorrect = false;
                if (splittedValuesByComma.length == 3) {
                    boolean firstValue = splittedValuesByComma[0].matches("^[0-9]|[1-9][0-9]|[1-3][0-5][0-9]|360$");
                    boolean secondValue = splittedValuesByComma[1].matches("^[0-9]|[1-9][0-9]|100$");
                    boolean thirdValue = splittedValuesByComma[2].matches("^[0-9]|[1-9][0-9]|100$");
                    if (firstValue && secondValue && thirdValue) {
                        isHSBStringCorrect = true;
                    }
                }
                if (!isGivenValueCorrect && !isPercentValueCorrect && !isHSBStringCorrect) {
                    ThrowWrongValueError(itemName, itemType, node);
                }
                break;
            }
            case ItemTypes.CONTACT: {
                boolean isGivenValueCorrect = givenValue.matches("^OPEN$|^CLOSED$");
                if (!isGivenValueCorrect) {
                    ThrowWrongValueError(itemName, itemType, node);
                }
                break;
            }
            case ItemTypes.DATETIME: {
                //since variables does not go inside this checking logic and for now datetimes only supports variables, it will throw errors.
                ThrowWrongValueError(itemName, itemType, node);
                break;
            }
            case ItemTypes.DIMMER: {
                boolean isGivenValueCorrect = givenValue.matches("^ON$|^OFF$|^INCREASE$|^DECREASE$");
                boolean isPercentValueCorrect = givenValue.matches("^[1-9][0-9]?$|^100$|^[0]$");
                if (!isGivenValueCorrect && !isPercentValueCorrect) {
                    ThrowWrongValueError(itemName, itemType, node);
                }
                break;
            }
            case ItemTypes.NUMBER: {
                boolean isGivenValueCorrect = givenValue.matches("[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)");
                if (!isGivenValueCorrect) {
                    ThrowWrongValueError(itemName, itemType, node);
                }
                break;
            }
            case ItemTypes.PLAYER: {
                boolean isGivenValueCorrect = givenValue.matches("^PLAY$|^PAUSE$|^NEXT$|^PREVIOUS$|^REWIND$|^FASTFORWARD$");
                if (!isGivenValueCorrect) {
                    ThrowWrongValueError(itemName, itemType, node);
                }
                break;
            }
            case ItemTypes.ROLLERSHUTTER: {
                boolean isGivenValueCorrect = givenValue.matches("^UP$|^DOWN$|^STOP$|^MOVE$");
                boolean isPercentValueCorrect = givenValue.matches("^[1-9][0-9]?$|^100$|^[0]$");
                if (!isGivenValueCorrect && !isPercentValueCorrect) {
                    ThrowWrongValueError(itemName, itemType, node);
                }
                break;
            }
            case ItemTypes.STRING: {
                boolean isGivenValueCorrect = givenValue.matches("^\".*\"$");
                if (!isGivenValueCorrect) {
                    ThrowWrongValueError(itemName, itemType, node);
                }
            }
            case ItemTypes.SWITCH: {
                boolean isGivenValueCorrect = givenValue.matches("^ON$|^OFF$");
                if (!isGivenValueCorrect) {
                    ThrowWrongValueError(itemName, itemType, node);
                }
            }
            default: break;
        }
    }

    public Boolean CheckIfExistsVariableByPattern(String pattern, Storage codeBuilder, Storage globalVariables) {
        Pattern p = Pattern.compile(pattern);
        String existingCode = codeBuilder.GetBuiltString();
        String existingGlobalVariables = globalVariables.GetBuiltString();
        Matcher codeMatcher = p.matcher(existingCode);
        Matcher globalVariablesMatcher = p.matcher(existingGlobalVariables);
        Boolean codeContainsVariable = codeMatcher.find();
        Boolean globalVariablesContainsVariable = globalVariablesMatcher.find();

        return codeContainsVariable || globalVariablesContainsVariable;
    }

    public void ValidateVariableExistance(String updateValue, Storage codeBuilder, Storage globalVariables) throws ExceptionHandler.CustomException {
        //check if variable already defined
        String regexPattern = "\\bvar \\b\\w+\\b " + updateValue + "\\b|\\bvar " + updateValue + "\\b";
        Boolean variableExists = CheckIfExistsVariableByPattern(regexPattern, codeBuilder, globalVariables);
        if (!variableExists) {
            exceptionHandler.ThrowCustomException("Variable with name: " + updateValue + " is not defined yet. You must use existing variables.");
        }
    }

    private void ThrowWrongValueError(String itemName, String itemType, Node node) throws ExceptionHandler.CustomException {
        NamedNodeMap attributesOfNode = node.getAttributes();
        Node idAttribute = attributesOfNode.getNamedItem("id");
        String id = idAttribute.getNodeValue();
        String nodeName = node.getNodeName();
        exceptionHandler.ThrowCustomException("Inside element: " + nodeName + " with id: " + id + " for the item: " + itemName + " which is type of: " + itemType + " " +
                "was given wrong value. Double check that and see a description under the input field what values per item type are allowed.");
    }
}
