package com.company.Operations;

import com.company.Handlers.ExceptionHandler;
import com.company.Storage.Storage;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by titas on 2017-04-17.
 */
public class SubprocessOperations {
    GlobalOperations globalOperations = new GlobalOperations();
    ExceptionHandler exceptionHandler = new ExceptionHandler();

    public void ProcessTimer(Node currentNode, Storage codeBuilder, Storage globalVariables, Storage imports) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String timerNameValue = "";
        String importValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "timername":
                    timerNameValue = node.getTextContent().trim();
                    break;
                case "importvalue":
                    importValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        String globals = globalVariables.GetBuiltString();
       //check if timer already defined
        if (globals.contains("var Timer " + timerNameValue)) {
            exceptionHandler.ThrowCustomException("Timer with name: " + timerNameValue + " already defined with that name.");
        }

        //add import if not already added
        String existingImports = imports.GetBuiltString();
        if (!existingImports.contains(importValue)) {
            imports.AppendString(importValue);
        }

        //add timer to global variables
        globalVariables.AppendString("var Timer " + timerNameValue);

        //assign timer to variable
        codeBuilder.AppendStringWithoutNewLine(timerNameValue + " = createTimer");
    }

    public void ProcessForeach(Node currentNode, Storage codeBuilder) {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String groupNameValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "groupname":
                    groupNameValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        codeBuilder.AppendString(groupNameValue + ".members.forEach[ item |");
    }
}
