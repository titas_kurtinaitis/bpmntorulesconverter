package com.company.Operations;

import com.company.Constants.ConditionStatementsTypes;
import com.company.Storage.Storage;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by titas on 2017-04-15.
 */
public class ConditionalOperations {
    GlobalOperations globalOperations = new GlobalOperations();

    public void ProcessIfStatement(Node currentNode, Storage codeBuilder, String statementType) {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String conditionValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "condition":
                    conditionValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }
        String ifStatementExpression = "";
        switch (statementType) {
            case ConditionStatementsTypes.ifstatement:
                ifStatementExpression = "if (" + conditionValue + ") {";
                break;
            case ConditionStatementsTypes.elseifstatement:
                ifStatementExpression = "else if (" + conditionValue + ") {";
                break;
            case ConditionStatementsTypes.elsestatement:
                ifStatementExpression = "else {";
                break;
            default:
                break;
        }

        codeBuilder.AppendString(ifStatementExpression);
    }
}
