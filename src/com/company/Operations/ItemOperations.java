package com.company.Operations;

import com.company.Handlers.ExceptionHandler;
import com.company.Storage.Storage;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by titas on 2017-04-13.
 */
public class ItemOperations {
    GlobalOperations globalOperations = new GlobalOperations();

    public void ProcessReceivedCommand(Node currentNode, Storage codeBuilder) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String itemNameValue = "";
        String commandValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "itemname":
                    itemNameValue = node.getTextContent().trim();
                    break;
                case "command":
                    commandValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //expected itemName and typeValue are split with | in dropdown values.
        String[] nameAndType = itemNameValue.split("\\|");
        String itemName = nameAndType[0];
        String itemType = nameAndType[1];
        globalOperations.CheckIfGivenValueIsAllowedForItemType(itemType, commandValue, itemName, currentNode);

        String commandExpression = "Item " + itemName + " received command " + commandValue;
        codeBuilder.AppendString(commandExpression);
    }

    public void ProcessReceivedUpdate(Node currentNode, Storage codeBuilder) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String itemNameValue = "";
        String updateValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "itemname":
                    itemNameValue = node.getTextContent().trim();
                    break;
                case "update":
                    updateValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //expected itemName and typeValue are split with | in dropdown values.
        String[] nameAndType = itemNameValue.split("\\|");
        String itemName = nameAndType[0];
        String itemType = nameAndType[1];
        globalOperations.CheckIfGivenValueIsAllowedForItemType(itemType, updateValue, itemName, currentNode);

        String updateExpression = "Item " + itemName + " received update " + updateValue;
        codeBuilder.AppendString(updateExpression);
    }

    public void ProcessChangedState(Node currentNode, Storage codeBuilder) throws ExceptionHandler.CustomException {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String itemNameValue = "";
        String statefromValue = "";
        String statetoValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "itemname":
                    itemNameValue = node.getTextContent().trim();
                    break;
                case "statefrom":
                    statefromValue = node.getTextContent().trim();
                    break;
                case "stateto":
                    statetoValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //expected itemName and typeValue are split with | in dropdown values.
        String[] nameAndType = itemNameValue.split("\\|");
        String itemName = nameAndType[0];
        String itemType = nameAndType[1];
        globalOperations.CheckIfGivenValueIsAllowedForItemType(itemType, statefromValue, itemName, currentNode);
        globalOperations.CheckIfGivenValueIsAllowedForItemType(itemType, statetoValue, itemName, currentNode);


        String stateChangedExpression = "";
        if (statefromValue != "" && statetoValue != "") {
            stateChangedExpression = "Item " + itemName + " changed from " + statefromValue + " to " + statetoValue;
        } else {
            stateChangedExpression = "Item " + itemName + " changed";
        }
        codeBuilder.AppendString(stateChangedExpression);
    }
}
