package com.company.Operations;

import com.company.Handlers.ExceptionHandler;
import com.company.Storage.Storage;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by titas on 2017-04-25.
 */
public class CustomOperations {
    GlobalOperations globalOperations = new GlobalOperations();

    public void ProcessCustomCode(Node currentNode, Storage codeBuilder, Storage imports) {
        NodeList inputParameters = globalOperations.GetChildElementsByTag(currentNode, "camunda:inputParameter");
        String codeLinesValue = "";
        String importsValue = "";
        for (int i = 0; i < inputParameters.getLength(); i++) {
            Node node = inputParameters.item(i);
            String attributeValue = globalOperations.GetAttributeValueByName(node, "name");
            switch (attributeValue) {
                case "customcodelines":
                    codeLinesValue = node.getTextContent().trim();
                    break;
                case "customimports":
                    importsValue = node.getTextContent().trim();
                    break;
                default:
                    break;
            }
        }

        //process imports
        String[] importValues = importsValue.split("\\s*,\\s*");
        for (String importValue : importValues) {
            if (!imports.GetBuiltString().contains(importValue)) {
                String importCodeLine = "import " + importValue;
                imports.AppendString(importCodeLine);
            }
        }

        //process custom code
        codeBuilder.AppendString(codeLinesValue);
    }
}
