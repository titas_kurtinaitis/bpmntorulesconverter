package com.company.Executors;

import com.company.Constants.ExecutionTypes;
import com.company.Constants.SubprocessTypes;
import com.company.Handlers.ExceptionHandler;
import com.company.Operations.*;
import com.company.Storage.Storage;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

/**
 * Created by titas on 2017-04-08.
 */
public class NodesExecuter {
    ExceptionHandler exceptionHandler = new ExceptionHandler();
    CronOperations cronOperations = new CronOperations();
    GlobalOperations globalOperations = new GlobalOperations();
    ItemOperations itemOperations = new ItemOperations();
    SendOperations sendOperations = new SendOperations();
    VariableOperations variableOperations = new VariableOperations();
    SubprocessOperations subprocessOperations = new SubprocessOperations();
    IntermediateCatchEventOperations intermediateCatchEventOperations = new IntermediateCatchEventOperations();
    CustomOperations customOperations = new CustomOperations();

    public NodeList GetElementsListByTag(Document doc, String tag) {
        NodeList nodesList = doc.getElementsByTagName(tag);
        return nodesList;
    }

    public String GetNameOfNode(Node node) {
        return node.getNodeName().trim();
    }

    public ArrayList<String> GetNextElementsOfNode(Node node) throws ExceptionHandler.CustomException {
        ArrayList<String> outgoingElements = new ArrayList<>();
        NodeList outgoingNodes = node.getChildNodes();
        for (int i = 0; i < outgoingNodes.getLength(); i++) {
            Node currentNode = outgoingNodes.item(i);
            if (currentNode.getNodeName().equals("bpmn:outgoing")) {
                outgoingElements.add(currentNode.getTextContent().trim());
            }
        }
        if (outgoingElements.isEmpty()) {
            exceptionHandler.ThrowCustomException("There are no outgoing elements.");
        }
        return outgoingElements;
    }

    public Node GetNodeById(Document doc, String id) throws ExceptionHandler.CustomException {
        NodeList incomingNodes = GetElementsListByTag(doc, "bpmn:incoming");
        Node parentNode = null;
        for (int i = 0; i < incomingNodes.getLength(); i++) {
            Node currentNode = incomingNodes.item(i);
            if (currentNode.getTextContent().trim().equals(id)) {
                parentNode = currentNode.getParentNode();
                return parentNode;
            }
        }
        if (parentNode == null) {
            exceptionHandler.ThrowCustomException("Id: " + id + ". Element does not exists.");
        }
        return null;
    }

    public String GetSequenceFlowTemplateType(Document doc, String id) throws ExceptionHandler.CustomException {
        NodeList nodeList = GetElementsListByTag(doc, "bpmn:sequenceFlow");
        Boolean foundSequenceFlow = false;
        for (int i = 0; i < nodeList.getLength(); i++) {
            NamedNodeMap attributesOfNode = nodeList.item(i).getAttributes();
            Node idAttributeNode = attributesOfNode.getNamedItem("id");
            if (idAttributeNode != null && idAttributeNode.getNodeValue().equals(id)) {
                foundSequenceFlow = true;
                Node camundaModelerTemplateNode = attributesOfNode.getNamedItem("camunda:modelerTemplate");
                if (camundaModelerTemplateNode != null) {
                    String camundaModelerTemplateValue = camundaModelerTemplateNode.getNodeValue().trim();
                    if (camundaModelerTemplateValue != "") {
                        return camundaModelerTemplateValue;
                    }
                    exceptionHandler.ThrowCustomException("bpmn:sequenceFlow with id: " + id + " has empty value as camunda modeler template");
                } else {
                    exceptionHandler.ThrowCustomException("bpmn:sequenceFlow with id: " + id + " does not have template attached when it should.");
                }
            }
        }
        if (!foundSequenceFlow) {
            exceptionHandler.ThrowCustomException("SequenceFlow with id: " + id + " was not found.");
        }
        return null;
    }

    public Node GetSequenceFlowNodeById(Document doc, String sequenceFlowId) throws ExceptionHandler.CustomException {
        NodeList nodeList = GetElementsListByTag(doc, "bpmn:sequenceFlow");
        Boolean foundSequenceFlow = false;
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            NamedNodeMap attributesOfNode = nodeList.item(i).getAttributes();
            Node idAttributeNode = attributesOfNode.getNamedItem("id");
            if (idAttributeNode != null && idAttributeNode.getNodeValue().equals(sequenceFlowId)) {
                foundSequenceFlow = true;
                return currentNode;
            }
        }
        if (!foundSequenceFlow) {
            exceptionHandler.ThrowCustomException("SequenceFlow with id: " + sequenceFlowId + " was not found.");
        }
        return null;
    }

    public String GetElementTagName(Node node)  {
        return node.getNodeName().trim();
    }

    public int GetCountOfNodesByTagNameAndParentNode(Document doc, String tag, Node parentNode) {
        int countOfNodes = 0;
        NodeList nodeListByTagName = GetElementsListByTag(doc, tag);
        for (int i = 0; i < nodeListByTagName.getLength(); i++) {
            Node currentNode = nodeListByTagName.item(i);
            Node parentNodeOfCurrent = currentNode.getParentNode();
            if (parentNodeOfCurrent.equals(parentNode)) {
                countOfNodes++;
            }
        }
        return countOfNodes;
    }

    public int GetCountOfOutgoingElements(Node node) throws ExceptionHandler.CustomException {
        return GetNextElementsOfNode(node).size();
    }

    public String GetNodeId(Node node, String entryId) throws ExceptionHandler.CustomException {
        NamedNodeMap attributesOfNode = node.getAttributes();
        Node idAttributeNode = attributesOfNode.getNamedItem("id");
        if (idAttributeNode == null) {
            exceptionHandler.ThrowCustomException("Node with entryId: " + entryId + " does not have id attribute");
        }
        return idAttributeNode.getNodeValue();
    }

    public boolean ExecuteByNodeTagAndId(Document doc, String id, String tag, Storage codeBuilder, Storage globalVariables, Storage imports) throws ExceptionHandler.CustomException {
        NodeList nodeList = GetElementsListByTag(doc, tag);
        Boolean foundElement = false;
        Boolean nodeHasValue = false;
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            NamedNodeMap attributesOfNode = currentNode.getAttributes();
            Node idAttributeNode = attributesOfNode.getNamedItem("id");
            if (idAttributeNode != null && idAttributeNode.getNodeValue().equals(id)) {
                foundElement = true;
                Node nameAtrributeNode = attributesOfNode.getNamedItem("camunda:modelerTemplate");
                //skip iteration if null value
                if (nameAtrributeNode == null) continue;
                String modelerTemplate = nameAtrributeNode.getNodeValue().trim();
                //for given code values from dropdown
                if (modelerTemplate.contains(ExecutionTypes.givencodefromdropdown)) {
                    globalOperations.ProcessGivenCodeFromDropdown(currentNode, codeBuilder);
                    nodeHasValue = true;
                    continue;
                }
                //for more specific cases
                switch (modelerTemplate) {
                    case ExecutionTypes.customcron:
                        cronOperations.ProcessCustomCron(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.receivedupdate:
                        itemOperations.ProcessReceivedUpdate(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.receivedcommand:
                        itemOperations.ProcessReceivedCommand(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.changedstate:
                        itemOperations.ProcessChangedState(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.sendcommand:
                        sendOperations.ProcessSendCommand(currentNode, codeBuilder, globalVariables);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.postupdate:
                        sendOperations.ProcessPostUpdate(currentNode, codeBuilder, globalVariables);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.sendemail:
                        sendOperations.ProcessSendEmail(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.callscript:
                        sendOperations.ProcessCallScript(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.predifinedtimevariable:
                        variableOperations.ProcessPredifinedTimeVariable(currentNode, codeBuilder, globalVariables, imports);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.createvariable:
                        variableOperations.ProcessCreateVariable(currentNode, codeBuilder, globalVariables, imports);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.assignvariable:
                        variableOperations.ProcessAssignVariable(currentNode, codeBuilder, globalVariables, imports);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.loginfo:
                        sendOperations.ProcessLogInfo(currentNode, codeBuilder, globalVariables);
                        nodeHasValue = true;
                        break;
                    case SubprocessTypes.timersubprocess:
                        subprocessOperations.ProcessTimer(currentNode, codeBuilder, globalVariables, imports);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.timerwaittime:
                        intermediateCatchEventOperations.ProcessIntermediateTimeCatchEvent(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    case SubprocessTypes.foreachsubprocess:
                        subprocessOperations.ProcessForeach(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.customcode:
                        customOperations.ProcessCustomCode(currentNode, codeBuilder, imports);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.sendcommandcurrentiteration:
                        sendOperations.ProcessSendCommandToCurrentItem(currentNode, codeBuilder, globalVariables);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.postupdatecurrentiteration:
                        sendOperations.ProcessPostUpdateToCurrentItem(currentNode, codeBuilder, globalVariables);
                        nodeHasValue = true;
                        break;
                    case ExecutionTypes.playsound:
                        sendOperations.ProcessPlaySound(currentNode, codeBuilder);
                        nodeHasValue = true;
                        break;
                    default: break;
                }
                return nodeHasValue;
            }
        }
        if (!foundElement) {
            exceptionHandler.ThrowCustomException("Element with id: " + id + " was not found.");
        }
        return nodeHasValue;
    }
}
