package com.company;

import com.company.Handlers.ExceptionHandler;
import com.company.Parsers.XmlParser;

import java.io.File;

public class Main {

    public static void main(String[] args) throws ExceptionHandler.CustomException {
        if (args.length > 0) {
            File file = new File(args[0]);
            String fileName = file.getName();
            if (fileName.indexOf(".") > 0) {
                String fileNameWithOutExt = fileName.substring(0, fileName.lastIndexOf("."));
                String saveFilePath = args[1] + "\\" + fileNameWithOutExt + ".rules";
                boolean onlyValidation = ((args.length > 2) ? Boolean.parseBoolean(args[2]) : false);
                new XmlParser(file, fileNameWithOutExt, saveFilePath, onlyValidation);
            }
        }
    }
}