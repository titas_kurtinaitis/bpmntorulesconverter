package com.company.Storage;

/**
 * Created by titas on 2017-04-08.
 */
public class Storage {
    private StringBuilder sb = new StringBuilder();

    public void AppendString(String givenString) {
        this.sb.append(givenString + "\n");
    }

    public void AppendStringWithoutNewLine(String givenString) { this.sb.append(givenString); }

    public void InsertNewLine() { this.sb.append("\n"); }

    public void AppendRuleName(String fileName) {this.sb.append("rule \"" + fileName + "\"\n");}

    public String GetBuiltString() {
        return this.sb.toString();
    }
}
