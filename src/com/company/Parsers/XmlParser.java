package com.company.Parsers;

import com.company.Constants.ConditionStatementsTypes;
import com.company.Constants.SubprocessTypes;
import com.company.Handlers.ExceptionHandler;
import com.company.Executors.NodesExecuter;
import com.company.Models.Condition;
import com.company.Operations.ConditionalOperations;
import com.company.Storage.Storage;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by titas on 2017-04-08.
 */
public class XmlParser {
    private Document doc;
    private Storage codeBuilder = new Storage();
    private Storage globalVariables = new Storage();
    private Storage imports = new Storage();
    private NodesExecuter nodesExecuter = new NodesExecuter();
    private ExceptionHandler exceptionHandler = new ExceptionHandler();
    private ConditionalOperations conditionalOperations = new ConditionalOperations();

    public XmlParser(File file, String fileName, String saveFilePath, boolean onlyValidation) throws ExceptionHandler.CustomException {
        codeBuilder.AppendRuleName(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            StartExecution(saveFilePath, onlyValidation);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void StartExecution(String saveFilePath, boolean onlyValidation) throws ExceptionHandler.CustomException, IOException {
        codeBuilder.AppendString("when");
        NodeList startEventNodes = nodesExecuter.GetElementsListByTag(this.doc, "bpmn:startEvent");

        //can be only one start event with parent node bpmn:process. Because subprocess can have start event. For example timers and etc.
        List<Node> startEventNodesWithoutParent = new ArrayList<Node>();
        for (int i = 0; i < startEventNodes.getLength(); i++) {
            Node currentIterationNode = startEventNodes.item(i);
            Node parentNode = currentIterationNode.getParentNode();
            if (parentNode.getNodeName().trim().equals("bpmn:process")) {
                startEventNodesWithoutParent.add(currentIterationNode);
            }
        }
        exceptionHandler.CheckIfStartEventsAreValid(startEventNodesWithoutParent);

        //it must be only one starEvent without parent
        ArrayList<String> outGoingElements = nodesExecuter.GetNextElementsOfNode(startEventNodes.item(0));
        //from start event it should be only one outgoing element
        if (outGoingElements.size() > 1) {
            exceptionHandler.ThrowCustomException("StartEvent should have only one outgoing element");
        }
        //after start event it should be executive gateway element
        Node nextNode = nodesExecuter.GetNodeById(this.doc, outGoingElements.get(0));
        String nodeName = nodesExecuter.GetNameOfNode(nextNode);
        if (!nodeName.equals("bpmn:exclusiveGateway")) {
            exceptionHandler.ThrowCustomException("After start event must be exclusiveGateway");
        }

        //fill when block with exclusive gateway routes
        ArrayList<String> outGoingElementsFromFirstGateway = nodesExecuter.GetNextElementsOfNode(nextNode);

        ArrayList<String> routesWhichHasValue = new ArrayList<>();
        //because it was gateway check for routes of gateway values
        for (int i = 0; i < outGoingElementsFromFirstGateway.size(); i++) {
            Boolean nodeHasValue = nodesExecuter.ExecuteByNodeTagAndId(doc, outGoingElementsFromFirstGateway.get(i), "bpmn:sequenceFlow", codeBuilder, globalVariables, imports);
            if (nodeHasValue) {
                routesWhichHasValue.add(outGoingElementsFromFirstGateway.get(i));
            }
        }

        if (routesWhichHasValue.size() != 1) {
            exceptionHandler.ThrowCustomException("Trigger after first gateway was not declared correctly. Make sure template assigned and exist one trigger for activity rule.");
        }

        //parent node of startEvent of process (this is a bit workaround)
        Node parentNodeOfProcessStartEvent = startEventNodes.item(0).getParentNode();

        //set to 1 because by default we know one end event exists after first gateway for when block
        int endEventsReached = 1;
        //find how many end events exist, so we can know we went through all path
        int numberOfEndEvents = nodesExecuter.GetCountOfNodesByTagNameAndParentNode(doc, "bpmn:endEvent", parentNodeOfProcessStartEvent);

        if (numberOfEndEvents < 2) {
            exceptionHandler.ThrowCustomException("End events are not correct.");
        }


        //Starts then block of the activity rule
        codeBuilder.AppendString("then");

        //for now only one route will have value after first gateway
        String nextNodeEntry = routesWhichHasValue.get(0);
        nextNode = nodesExecuter.GetNodeById(doc, nextNodeEntry);
        String nextNodeId = nodesExecuter.GetNodeId(nextNode, nextNodeEntry);

        //start processing elements
        GoFromStartEventToEndEvent(nextNode, endEventsReached, numberOfEndEvents, nextNodeId);


        //then block ended, needs end to end actvity rule
        codeBuilder.AppendString("end");

        //add line if there is some imports or global values
        if (!imports.GetBuiltString().isEmpty()) {
            imports.InsertNewLine();
        }
        if (!imports.GetBuiltString().isEmpty()) {
            globalVariables.InsertNewLine();
        }

        //save to file
        if (!onlyValidation) {
            String content = imports.GetBuiltString() + globalVariables.GetBuiltString() + codeBuilder.GetBuiltString();
            File f = new File(saveFilePath);
            if(f.exists() && !f.isDirectory()) {
                Files.write(Paths.get(saveFilePath), content.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
            } else {
                Files.write(Paths.get(saveFilePath), content.getBytes(), StandardOpenOption.CREATE);
            }
            System.out.println(content);
        }
    }

    private Node GoThroughExclusiveGatewayRoute(String outgoingElementId, int endEventsReached) throws ExceptionHandler.CustomException {
        Node returnedNode = null;
        Node nextNode = nodesExecuter.GetNodeById(doc, outgoingElementId);
        String nextNodeId = nodesExecuter.GetNodeId(nextNode, outgoingElementId);
        boolean wentThroughAllRoute = false;
        while(!wentThroughAllRoute) {
            int countOfOutgoing = 0;
            //additional check needed because endEvent does not have outgoing elements
            if (!nodesExecuter.GetNameOfNode(nextNode).equals("bpmn:endEvent")) {
                countOfOutgoing = nodesExecuter.GetCountOfOutgoingElements(nextNode);
            }
            //TODO for now assume that if gateway met it is the end of route. In the future extend for additional exclusiveGateways
            String nodeName = nodesExecuter.GetNameOfNode(nextNode);
            if (nodeName.equals("bpmn:exclusiveGateway") && countOfOutgoing == 1) {
                codeBuilder.AppendStringWithoutNewLine("} ");
                wentThroughAllRoute = true;
                //asume one outgoing element
                String outgoingElementWhenGateWayProcessed = nodesExecuter.GetNextElementsOfNode(nextNode).get(0);
                returnedNode = nodesExecuter.GetNodeById(doc, outgoingElementWhenGateWayProcessed);
            } else if (nodeName.equals("bpmn:exclusiveGateway") && countOfOutgoing > 1) {
                nextNode = ProcessExclusiveGateway(nextNode, endEventsReached);
                continue;
            }
            if (nodeName.equals("bpmn:endEvent")) {
                codeBuilder.AppendStringWithoutNewLine("} ");
                wentThroughAllRoute = true;
                endEventsReached++;
                continue;
            }
            if (nodeName.equals("bpmn:subProcess") && countOfOutgoing == 1) {
                nextNode = ProcessSubProcess(nextNode, nextNodeId);
                continue;
            }

            //saves value of task if not exclusive gateway or subprocess
            if (!nodeName.equals("bpmn:exclusiveGateway")) {
                String elementTagName = nodesExecuter.GetElementTagName(nextNode);
                nodesExecuter.ExecuteByNodeTagAndId(doc, nextNodeId, elementTagName, codeBuilder, globalVariables, imports);
            }
            outgoingElementId =  nodesExecuter.GetNextElementsOfNode(nextNode).get(0);
            //TODO assume for now that outGoingElements only one. This should be handled in the future.
            nextNode = nodesExecuter.GetNodeById(doc, outgoingElementId);
            nextNodeId = nodesExecuter.GetNodeId(nextNode, outgoingElementId);
            outgoingElementId = nodesExecuter.GetNodeId(nextNode, outgoingElementId);
        }
        return returnedNode;
    }

    private Node ProcessExclusiveGateway(Node node, int endEventsReached) throws ExceptionHandler.CustomException {
        ArrayList<String> outGoingElements;
        ArrayList<Condition> outGoingConditions = new ArrayList<>();
        Node nextNode = null;
        outGoingElements = nodesExecuter.GetNextElementsOfNode(node);
        for (int i = 0; i < outGoingElements.size(); i++) {
            String sequenceFlowTemplateType = nodesExecuter.GetSequenceFlowTemplateType(doc, outGoingElements.get(i));
            if (sequenceFlowTemplateType != null) {
                Condition condition = new Condition();
                condition.setId(outGoingElements.get(i));
                condition.setType(sequenceFlowTemplateType);
                outGoingConditions.add(condition);
            }
        }
        long countOfIfStatements = outGoingConditions.stream().filter(o -> ConditionStatementsTypes.ifstatement.equals(o.getType())).count();
        long countOfIfElseStatements = outGoingConditions.stream().filter(o -> ConditionStatementsTypes.elseifstatement.equals(o.getType())).count();
        long countOfElseStatements = outGoingConditions.stream().filter(o -> ConditionStatementsTypes.elsestatement.equals(o.getType())).count();
        exceptionHandler.CheckIfExclusiveGatewayIsCorrect(countOfIfStatements, countOfIfElseStatements, countOfElseStatements);
        //when gateway is correct
        List<String> ifStatementIds = outGoingConditions.stream()
                .filter(o -> ConditionStatementsTypes.ifstatement.equals(o.getType()))
                .map(o -> o.getId())
                .collect(Collectors.toList());
        List<String> elseIfStatementIds = outGoingConditions.stream()
                .filter(o -> ConditionStatementsTypes.elseifstatement.equals(o.getType()))
                .map(o -> o.getId())
                .collect(Collectors.toList());
        List<String> elseStatementIds = outGoingConditions.stream()
                .filter(o -> ConditionStatementsTypes.elsestatement.equals(o.getType()))
                .map(o -> o.getId())
                .collect(Collectors.toList());
        //if statements
        for (String ifStatementId: ifStatementIds) {
            //needs new line to insert if there is more than one if statement
            if (ifStatementIds.indexOf(ifStatementId) > 0) {
                codeBuilder.InsertNewLine();
            }
            Node currentNode = nodesExecuter.GetSequenceFlowNodeById(doc, ifStatementId);
            conditionalOperations.ProcessIfStatement(currentNode, codeBuilder, ConditionStatementsTypes.ifstatement);
            nextNode = GoThroughExclusiveGatewayRoute(ifStatementId, endEventsReached);
        }
        //elseif statements
        for (String elseifStatementId: elseIfStatementIds){
            Node currentNode = nodesExecuter.GetSequenceFlowNodeById(doc, elseifStatementId);
            conditionalOperations.ProcessIfStatement(currentNode, codeBuilder, ConditionStatementsTypes.elseifstatement);
            nextNode = GoThroughExclusiveGatewayRoute(elseifStatementId, endEventsReached);
        }
        //else statements
        for (String elseStatementId: elseStatementIds){
            Node currentNode = nodesExecuter.GetSequenceFlowNodeById(doc, elseStatementId);
            conditionalOperations.ProcessIfStatement(currentNode, codeBuilder, ConditionStatementsTypes.elsestatement);
            nextNode = GoThroughExclusiveGatewayRoute(elseStatementId, endEventsReached);
        }
        //needs new line to insert because end of the statement
        codeBuilder.InsertNewLine();
        return nextNode;
    }

    private Node ProcessSubProcess(Node subProcessNode, String nodeEntryId) throws ExceptionHandler.CustomException {
        //find start event nodes of subProcess. It has to be one subprocess startevent node
        NodeList startEventNodes = nodesExecuter.GetElementsListByTag(this.doc, "bpmn:startEvent");
        List<Node> processStartEventNodes = new ArrayList<Node>();
        for (int i = 0; i < startEventNodes.getLength(); i++) {
            Node currentIterationNode = startEventNodes.item(i);
            Node parentNode = currentIterationNode.getParentNode();
            if (parentNode.equals(subProcessNode)) {
                processStartEventNodes.add(currentIterationNode);
            }
        }

        //subprocess should have only one bpmn:startevent node
        exceptionHandler.CheckIfStartEventsAreValid(processStartEventNodes);


        //process subprocess template
        String nodeId = nodesExecuter.GetNodeId(subProcessNode, nodeEntryId);
        String elementTagName = nodesExecuter.GetElementTagName(subProcessNode);
        Boolean nodeHasValue = nodesExecuter.ExecuteByNodeTagAndId(doc, nodeId, elementTagName, codeBuilder, globalVariables, imports);
        if (!nodeHasValue) {
            exceptionHandler.ThrowCustomException("Subprocess with id: " + nodeId + " does not have template assigned.");
        }

        //initial data needed to go from start to end event in subProcess
        int eventsReached = 0;
        int numberOfEndEvents = nodesExecuter.GetCountOfNodesByTagNameAndParentNode(doc, "bpmn:endEvent", subProcessNode);
        //only one startEvent node is possible, so index is 0
        ArrayList<String> outGoingElementsFromStartEventNode = nodesExecuter.GetNextElementsOfNode(processStartEventNodes.get(0));
        Node nextNode = nodesExecuter.GetNodeById(doc, outGoingElementsFromStartEventNode.get(0));
        String nextNodeId = nodesExecuter.GetNodeId(nextNode, outGoingElementsFromStartEventNode.get(0));

        //if it is timer subprocess should have intermediateCatchEvent for timer
        NamedNodeMap subprocessAttributes = subProcessNode.getAttributes();
        String idOfTemplate = subprocessAttributes.getNamedItem("camunda:modelerTemplate").getNodeValue();
        if (idOfTemplate.equals(SubprocessTypes.timersubprocess)) {
            Boolean timerSubprocessIsCorrect = false;
            if (nextNode.getNodeName().equals("bpmn:intermediateCatchEvent")) {
                NodeList childNodesOfTimerCatchEvent = nextNode.getChildNodes();
                for (int i = 0; i < childNodesOfTimerCatchEvent.getLength(); i++) {
                    Node currentNode = childNodesOfTimerCatchEvent.item(i);
                    String currentNodeName = currentNode.getNodeName();
                    if (currentNodeName.equals("bpmn:timerEventDefinition")) {
                        timerSubprocessIsCorrect = true;
                        break;
                    }
                }
            }
            if (!timerSubprocessIsCorrect) {
                exceptionHandler.ThrowCustomException("Subprocess which is type of timer must have bpmn:intermediateCatchEvent timer element after start event inside subprocess. " +
                        "This value is required to define timer's wait time.");
            }
        }

        //if it is foreach subprocess. Must have loop child node bpmn:standardLoopCharacteristics
        if (idOfTemplate.equals(SubprocessTypes.foreachsubprocess)) {
            Boolean forEachSubprocessIsCorrect = false;
            NodeList childNodesOfSubprocess = subProcessNode.getChildNodes();
            for (int i = 0; i < childNodesOfSubprocess.getLength(); i++) {
                Node currentNode = childNodesOfSubprocess.item(i);
                String currentNodeName = currentNode.getNodeName();
                if (currentNodeName.equals("bpmn:standardLoopCharacteristics")) {
                    forEachSubprocessIsCorrect = true;
                    break;
                }
            }
            if (!forEachSubprocessIsCorrect) {
                exceptionHandler.ThrowCustomException("Subprocess which iterates through each group member must be defined as loop.");
            }
        }

        //go from start to end event in subprocess
        GoFromStartEventToEndEvent(nextNode, eventsReached, numberOfEndEvents, nextNodeId);

        //Get next node which goes after subprocess
        ArrayList<String> outGoingElementsOfSubprocess = nodesExecuter.GetNextElementsOfNode(subProcessNode);
        if (outGoingElementsOfSubprocess.size() != 1) {
            exceptionHandler.ThrowCustomException("Subprocess must have ONE outgoing element.");
        }

        //needs add end symbol to subprocess
        codeBuilder.AppendString("]");

        Node nodeAfterSubprocess = nodesExecuter.GetNodeById(doc, outGoingElementsOfSubprocess.get(0));
        return nodeAfterSubprocess;
    }

    private void GoFromStartEventToEndEvent(Node nextNode, int reachedEndEvents, int endEventsTotal, String idOfNextNode) throws ExceptionHandler.CustomException {
        int endEventsReached = reachedEndEvents;
        int numberOfEndEvents = endEventsTotal;
        String nextNodeId = idOfNextNode;
        //go through all paths for then block of activity rule
        while (endEventsReached < numberOfEndEvents) {
            if (nextNode != null && nodesExecuter.GetNameOfNode(nextNode).equals("bpmn:endEvent")) {
                endEventsReached++;
                continue;
            }
            if (nextNode != null && nodesExecuter.GetNameOfNode(nextNode).equals("bpmn:exclusiveGateway")) {
                nextNode = ProcessExclusiveGateway(nextNode, endEventsReached);
                continue;
            }
            if (nextNode != null && nodesExecuter.GetNameOfNode(nextNode).equals("bpmn:subProcess")) {
                nextNode = ProcessSubProcess(nextNode, nextNodeId);
                continue;
            }

            String elementTagName = nodesExecuter.GetElementTagName(nextNode);
            Boolean nodeHasValue = nodesExecuter.ExecuteByNodeTagAndId(doc, nextNodeId, elementTagName, codeBuilder, globalVariables, imports);
            if (!nodeHasValue) {
                exceptionHandler.ThrowCustomException("Element " + elementTagName + " with id " + nextNodeId + " does not have template assigned");
            }
            //TODO Make sure here should be only one outgoing element
            ArrayList<String> outGoingElements = nodesExecuter.GetNextElementsOfNode(nextNode);
            if (outGoingElements.size() > 1) {
                exceptionHandler.ThrowCustomException("Element with id: " + nextNodeId + " cannot have more than one outgoing element");
            }
            String nextNodeEntry = outGoingElements.get(0);
            nextNode = nodesExecuter.GetNodeById(doc, nextNodeEntry);
            nextNodeId = nodesExecuter.GetNodeId(nextNode, nextNodeEntry);
        }
    }
}