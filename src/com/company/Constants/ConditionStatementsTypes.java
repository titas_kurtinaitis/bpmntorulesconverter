package com.company.Constants;

/**
 * Created by titas on 2017-04-15.
 */
public final class ConditionStatementsTypes {
    public static final String ifstatement = "ifstatement";
    public static final String elseifstatement = "elseifstatement";
    public static final String elsestatement = "elsestatement";

    public ConditionStatementsTypes() {}
}
