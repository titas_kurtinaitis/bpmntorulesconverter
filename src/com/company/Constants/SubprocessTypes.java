package com.company.Constants;

/**
 * Created by titas on 2017-04-17.
 */
public final class SubprocessTypes {
    public static final String timersubprocess = "timersubprocess";
    public static final String foreachsubprocess = "foreachsubprocess";

    private SubprocessTypes() { }
}
