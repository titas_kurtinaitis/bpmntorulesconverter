package com.company.Constants;

/**
 * Created by titas on 2017-04-12.
 */
public final class ExecutionTypes {
    public static final String customcron = "customcron";
    public static final String givencodefromdropdown = "givencodefromdropdown";
    public static final String receivedcommand = "receivedcommand";
    public static final String receivedupdate = "receivedupdate";
    public static final String changedstate = "changedstate";
    public static final String sendcommand = "sendcommand";
    public static final String postupdate = "postupdate";
    public static final String sendemail = "sendemail";
    public static final String callscript = "callscript";
    public static final String predifinedtimevariable = "predifinedtimevariable";
    public static final String createvariable = "createvariable";
    public static final String assignvariable = "assignvariable";
    public static final String loginfo = "loginfo";
    public static final String timerwaittime = "timerwaittime";
    public static final String customcode = "customcode";
    public static final String sendcommandcurrentiteration = "sendcommandcurrentiteration";
    public static final String postupdatecurrentiteration = "postupdatecurrentiteration";
    public static final String playsound = "playsound";


    private ExecutionTypes() { }
}
