package com.company.Constants;

/**
 * Created by titas on 2017-04-26.
 */
public final class ItemTypes {
    public static final String COLOR = "COLOR";
    public static final String CONTACT = "CONTACT";
    public static final String DATETIME = "DATETIME";
    public static final String DIMMER = "DIMMER";
    public static final String NUMBER = "NUMBER";
    public static final String PLAYER = "PLAYER";
    public static final String ROLLERSHUTTER = "ROLLERSHUTTER";
    public static final String STRING = "STRING";
    public static final String SWITCH = "SWITCH";

    public ItemTypes() {}
}
