package com.company.Models;

/**
 * Created by titas on 2017-04-15.
 */
public class Condition {
    private String id;
    private String type;

    public String getId() { return this.id; }
    public void setId(String id) { this.id = id; }

    public String getType() { return this.type; }
    public void setType(String type) { this.type = type; }
}
